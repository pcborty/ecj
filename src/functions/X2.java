package functions;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import main.RegressionData;
import main.RegressionProblem;

public class X2 extends GPNode {

    public String toString() {
        return "x2";
    }

    public int expectedChildren() {
        return 0;
    }

    @Override
    public void eval(final EvolutionState state,
                     final int thread,
                     final GPData input,
                     final ADFStack stack,
                     final GPIndividual individual,
                     final Problem problem) {

        RegressionData rd = ((RegressionData) (input));
        rd.x = ((RegressionProblem) problem).currentX2;
    }
}
