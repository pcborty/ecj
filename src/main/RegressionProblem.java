package main;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.gp.koza.KozaFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Float.parseFloat;

public class RegressionProblem extends GPProblem implements SimpleProblemForm {

    public float currentX1, currentX2;
    public ArrayList<float[]> inputX = new ArrayList<>();
    public ArrayList<Float> inputY = new ArrayList<>();

    @Override
    public void setup(EvolutionState state, Parameter base) {
        super.setup(state, base);

        if (!(input instanceof RegressionData)) {
            state.output.fatal("GPData class must subclass from " + RegressionData.class,
                    base.push(P_DATA), null);
        }

        try {
            loadData();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void evaluate(final EvolutionState evolutionState, final Individual individual,
                         final int subPopulation, final int threadNum) {

        if (!individual.evaluated) {

            RegressionData input = (RegressionData) (this.input);
            GPIndividual ind = (GPIndividual) individual;

            int hits = 0;
            float sum = 0;
            float expectedResult, result;

            for (int i = 0; i < inputX.size(); i++) {
                float[] currentX = inputX.get(i);
                currentX1 = currentX[0];
                currentX2 = currentX[1];

                expectedResult = inputY.get(i);

                ind.trees[0].child.eval(evolutionState, threadNum, input, stack, ind, this);

                result = Math.abs(expectedResult - input.x);
                if (result <= 0.01) {
                    hits++;
                }

                sum += result;
            }

            KozaFitness f = ((KozaFitness) ind.fitness);
            f.setStandardizedFitness(evolutionState, sum);
            f.hits = hits;

            ind.evaluated = true;
        }
    }

    @Override
    public void describe(EvolutionState evolutionState, Individual individual,
                         int subPopulation, int threadNum, int log) {

        super.describe(evolutionState, individual, subPopulation, threadNum, log);

        RegressionData input = (RegressionData) (this.input);
        GPIndividual ind = (GPIndividual) individual;

        int hits = 0;
        float sum = 0;
        float expectedResult, result;

        for (int i = 0; i < inputX.size(); i++) {
            float[] currentX = inputX.get(i);
            currentX1 = currentX[0];
            currentX2 = currentX[1];

            expectedResult = inputY.get(i);

            ind.trees[0].child.eval(evolutionState, threadNum, input, stack, ind, this);

            result = Math.abs(expectedResult - input.x);
            if (result <= 0.01) {
                hits++;
            }

            sum += result;
        }

        KozaFitness f = ((KozaFitness) ind.fitness);
        f.setStandardizedFitness(evolutionState, sum);
        f.hits = hits;
    }

    private void loadData() throws FileNotFoundException {
        File file = new File("src/main/file/data3.txt");

        Scanner scan = new Scanner(file);

        while (scan.hasNextLine()) {
            String[] data = scan.nextLine().split("\t");

            inputX.add(new float[]{parseFloat(data[0]), parseFloat(data[1])});
            inputY.add(parseFloat(data[2]));
        }
    }
}
