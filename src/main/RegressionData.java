package main;

import ec.gp.GPData;

public class RegressionData extends GPData {

    public float x;

    public void copyTo(final GPData gpd) {
        ((RegressionData) gpd).x = x;
    }
}
